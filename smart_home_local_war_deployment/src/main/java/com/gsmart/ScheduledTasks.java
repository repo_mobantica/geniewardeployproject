package com.gsmart;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import com.amazonaws.util.json.JSONObject;
import com.gsmart.entity.VersionType;
import com.gsmart.repository.VersionTypeRepository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import MqttReadDataPaho.JdbcConnection;
import MqttReadDataPaho.MqttIntConnectionToHub;


@Service
public class ScheduledTasks {

	@Autowired
	VersionTypeRepository versiontypeRepository;

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

	private static Long userId = 0l;

	private static final SimpleDateFormat dateFormatByDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
	private static final SimpleDateFormat dateFormatByTime = new SimpleDateFormat("HH:mm:00");

	@Async
	@Scheduled(fixedRate = 86400000)
	public void downloadWarFunction() throws IOException  {
	
		try {

			List<VersionType> versiontypelist=versiontypeRepository.findAll();
			double currentVersion=versiontypelist.get(0).getVersionType();
			double currentVersionType;
			String currentVersionName="";

			URL urlForGetRequest = new URL("http://gsmarthome.genieiot.in/versionType/getCurrentVersionType");
			String readLine = null;
			HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
			conection.setRequestMethod("POST");
			//conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
			int responseCode = conection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(
						new InputStreamReader(conection.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in .readLine()) != null) {
					response.append(readLine);
				} in .close();
				// print result

				JSONObject jsonObject = new JSONObject(response.toString());
				currentVersionType = Double.parseDouble(jsonObject.get("versionType").toString());
				currentVersionName = jsonObject.get("versionName").toString();
				if(Double.compare(currentVersionType, currentVersion) > 0) 
				{
					URL url;
					URLConnection con;
					DataInputStream dis; 
					FileOutputStream fos; 
					byte[] fileData;  

					try {

		                url = new URL("http://gsmartapp.genieiot.in/localWarFile/smart_home_local.war");
					
						con = url.openConnection(); // open the url connection.
						dis = new DataInputStream(con.getInputStream());
						fileData = new byte[con.getContentLength()]; 

						for (int q = 0; q < fileData.length; q++) { 
							fileData[q] = dis.readByte();
						}
						
						dis.close(); // close the data input stream
						
						//D:/ProjectWarFile/WarFor_71/new/smart_home_local.war
						///var/lib/tomcat8/webapps/smart_home_local.war
						fos = new FileOutputStream(new File("/var/lib/tomcat8/webapps/smart_home_local.war")); //FILE Save Location goes here
						fos.write(fileData);  // write out the file we want to save.

						fos.close(); 

						VersionType version=new VersionType();
						version.setId(1);
						version.setVersionName(currentVersionName);
						version.setVersionType(currentVersionType);
						version.setCreateDate(new Date());
						versiontypeRepository.save(version);

						
						
					}
					catch(Exception m) {
						m.printStackTrace();
					}
				}
				//GetAndPost.POSTRequest(response.toString());
			} else {
				System.out.println("GET NOT WORKED");
			}


		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	@Async
	@Scheduled(fixedRate = 1000*60)
	public void call() throws IOException  {
		
		try {
			//MqttlConnection.mqttConnectionInternet.disconnect();
			MqttIntConnectionToHub.getObj().getMqttConnection();

			System.out.println("check connect");
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}


	@Async
	@Scheduled(fixedRate = 3600000*5)
	public void sendLocalConnection() throws IOException  {
		

		JdbcConnection JdbcConnection=new JdbcConnection();
		String home_id=JdbcConnection.HomeId();
		try {
			String msg="wish/checkLocalHomeConnection/2/"+home_id;
			MqttMessage messageOff = new MqttMessage(msg.toString().getBytes());
			messageOff.setQos(2);
			MqttIntConnectionToHub.getObj().getMqttConnection().publish("GenieHomeId", messageOff);
			System.out.println("send msg ");
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

}
