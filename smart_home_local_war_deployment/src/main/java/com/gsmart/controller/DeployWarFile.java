package com.gsmart.controller;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.util.json.JSONObject;
import com.gsmart.entity.VersionType;
import com.gsmart.repository.VersionTypeRepository;

import MqttReadDataPaho.JdbcConnection;

public class DeployWarFile {

	@Autowired
	VersionTypeRepository versiontypeRepository;

	public String DeployWarFile()
	{

		try {

			//List<VersionType> versiontypelist=versiontypeRepository.findAll();
			//double currentVersion=versiontypelist.get(0).getVersionType();
			JdbcConnection jdbcConnection=new JdbcConnection();
			double localVersionType=jdbcConnection.LocalVersionType();
			double currentVersionType;
			String currentVersionName="";

			URL urlForGetRequest = new URL("http://gsmarthome.genieiot.in/versionType/getCurrentVersionType");
			String readLine = null;
			HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
			conection.setRequestMethod("POST");
			//conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
			int responseCode = conection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(
						new InputStreamReader(conection.getInputStream()));
				StringBuffer response = new StringBuffer();
				while ((readLine = in .readLine()) != null) {
					response.append(readLine);
				} in .close();
				// print result

				JSONObject jsonObject = new JSONObject(response.toString());
				currentVersionType = Double.parseDouble(jsonObject.get("versionType").toString());
				currentVersionName = jsonObject.get("versionName").toString();
				if(Double.compare(currentVersionType, localVersionType) > 0) 
				{
					URL url;
					URLConnection con;
					DataInputStream dis; 
					FileOutputStream fos; 
					byte[] fileData;  

					try {

						url = new URL("http://gsmartapp.genieiot.in/localWarFile/smart_home_local.war");

						con = url.openConnection(); // open the url connection.
						dis = new DataInputStream(con.getInputStream());
						fileData = new byte[con.getContentLength()]; 

						for (int q = 0; q < fileData.length; q++) { 
							fileData[q] = dis.readByte();
						}

						dis.close(); // close the data input stream

						//D:/ProjectWarFile/WarFor_71/new/smart_home_local.war
						///var/lib/tomcat8/webapps/smart_home_local.war
						//fos = new FileOutputStream(new File("D:/ProjectWarFile/WarFor_71/new/smart_home_local.war")); 
						fos = new FileOutputStream(new File("/var/lib/tomcat8/webapps/smart_home_local.war")); //FILE Save Location goes here

						fos.write(fileData);  // write out the file we want to save.

						fos.close(); 

						jdbcConnection.UpdateVersionType(currentVersionType, currentVersionName);

						/*
						VersionType version=new VersionType();
						version.setId(1);
						version.setVersionName(currentVersionName);
						version.setVersionType(currentVersionType);
						version.setCreateDate(new Date());
						versiontypeRepository.save(version);

						 */

						return "1";
					}
					catch(Exception m) {
						m.printStackTrace();

						return "0";
					}
				}

				return "3";
				//GetAndPost.POSTRequest(response.toString());
			} else {

				System.out.println("GET NOT WORKED");
				return "0";
			}


		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return "0";
		}

	}

}
