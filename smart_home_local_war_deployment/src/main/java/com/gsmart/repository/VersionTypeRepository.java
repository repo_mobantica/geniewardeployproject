package com.gsmart.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gsmart.entity.VersionType;

public interface VersionTypeRepository extends CrudRepository<VersionType, Long> {

	List findAll();
}
