package com.gsmart;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.logging.FileHandler;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableWebMvc
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.gsmart" })
public class Application extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	static FileHandler fileHandler = null;

	public static void main(String[] args) {	

		//PropertiesConfigurator is used to configure logger from properties file
		PropertyConfigurator.configure("log4j.properties");
		logger.debug("****************I M Debug LOG ****************************");
		logger.info("****************I M Info LOG ****************************");
		logger.error("****************I M Error LOG ****************************");

		try {
			fileHandler  = new FileHandler("./javacodegeeks.log");
			logger.info("File handler Code called");
		} catch (SecurityException e) {
			logger.error("Security Exception in Main Application");
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IO Exception in Main Application");
			e.printStackTrace();
		}

		//this was implemented to send notification whenever application restarts
		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);	

	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		return application.sources(Application.class);
	}

	@Bean
	public SecureRandom secureRandom() {
		return new SecureRandom();
	}

	/*
	 * @Bean public void refresh() { return new Refresh().setCallBack(); }
	 */

	@Bean()
	public ThreadPoolTaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.setPoolSize(5);
		return taskScheduler;
	}

	/*
	 * @Bean public static PropertyPlaceholderConfigurer
	 * propertyPlaceholderConfigurer() { PropertyPlaceholderConfigurer ppc = new
	 * PropertyPlaceholderConfigurer(); ppc.setLocations(new Resource[] { new
	 * ClassPathResource("/amazon.properties") }); return ppc; }
	 */

	/*
	 * @Bean public AWSCredentials credential() { return new
	 * BasicAWSCredentials(awsId, awsKey); }
	 * 
	 * @Bean public AmazonS3 s3client() { return new
	 * AmazonS3Client(credential()); }
	 */

	/*
	 * @Bean public JavaMailSender javaMailSender() { JavaMailSenderImpl
	 * mailSender = new JavaMailSenderImpl(); Properties mailProperties = new
	 * Properties(); mailProperties.put("mail.smtp.auth", auth);
	 * mailProperties.put("mail.smtp.starttls.enable", starttls);
	 * mailProperties.put("mail.smtp.starttls.required", true);
	 * mailSender.setJavaMailProperties(mailProperties);
	 * mailSender.setHost(host); mailSender.setPort(port);
	 * mailSender.setProtocol(protocol); mailSender.setUsername(username);
	 * mailSender.setPassword(password); return mailSender; }
	 */

}
