package com.gsmart.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Home {

	@Id   
    private Long id;
	
    private String homeName;
    
    private Integer isArmed;
    
    private Integer isBlocked;
    
    @JsonIgnore
	@OneToMany(mappedBy = "home", cascade = CascadeType.ALL,orphanRemoval=true)
    private List<UserDetails> userList = new ArrayList<UserDetails>();
    
    @JsonIgnore
    @OneToMany(mappedBy = "home", cascade = CascadeType.ALL,orphanRemoval=true)
    private List<Room> roomList=new ArrayList<Room>();

	public Home() {
	}
	
	public Home(String homeName) {
		this.homeName = homeName;
	}
	public Home(String homeName, List<UserDetails> userList,
			List<Room> roomList) {
		this.homeName = homeName;
		this.userList = userList;
		this.roomList = roomList;
	}

	public List<Room> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<Room> roomList) {
		this.roomList = roomList;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getHomeName() {
		return homeName;
	}

	public void setHomeName(String homeName) {
		this.homeName = homeName;
	}


	public List<UserDetails> getUserList() {
		return userList;
	}

	public void setUserList(List<UserDetails> userList) {
		this.userList = userList;
	}

    public Integer getIsArmed() {
		return isArmed;
	}

	public void setIsArmed(Integer isArmed) {
		this.isArmed = isArmed;
	}

	public Integer getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Integer isBlocked) {
		this.isBlocked = isBlocked;
	}

	@Override
	public String toString() {
		return "Home [id=" + id + ", homeName=" + homeName + ", isArmed=" + isArmed + ", isBlocked=" + isBlocked
				+ ", userList=" + userList + ", roomList=" + roomList + "]";
	}
	
}
