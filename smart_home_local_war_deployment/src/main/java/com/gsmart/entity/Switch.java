package com.gsmart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Switch {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String switchIdentifier;

	private Integer switchNumber;

	private String switchType;

	private String dimmerStatus;

	private String dimmerValue;

	private String switchStatus;

	private String lockStatus;

	private String hideStatus;

	private String switchName;
	
	private String lockCode;
	
	private Integer switchImageId;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "IOTPRODUCTID", nullable = false)
	private IOTProduct iotProduct;

	public Switch() {
	}

	public Switch(String switchName) {
		this.switchName = switchName;
	}

	public Switch(String switchIdentifier, String switchType, String dimmerStatus, String dimmerValue,
			String switchStatus, String switchName) {
		super();
		this.switchIdentifier = switchIdentifier;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
		this.switchName = switchName;
	}

	public Switch(Long id, String switchIdentifier, Integer switchNumber, String switchType, String dimmerStatus,
			String dimmerValue, String switchStatus, IOTProduct iotProduct, String lockStatus, String hideStatus) {
		this.id = id;
		this.switchIdentifier = switchIdentifier;
		this.switchNumber = switchNumber;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
		this.iotProduct = iotProduct;
		this.lockStatus = lockStatus;
		this.hideStatus = hideStatus;

	}
	
	public Switch(Long id, String switchIdentifier, Integer switchNumber, String switchType, String dimmerStatus,
			String dimmerValue, String switchStatus, IOTProduct iotProduct, String lockStatus, String hideStatus, String lockCode) {
		this.id = id;
		this.switchIdentifier = switchIdentifier;
		this.switchNumber = switchNumber;
		this.switchType = switchType;
		this.dimmerStatus = dimmerStatus;
		this.dimmerValue = dimmerValue;
		this.switchStatus = switchStatus;
		this.iotProduct = iotProduct;
		this.lockStatus = lockStatus;
		this.hideStatus = hideStatus;
		this.lockCode = lockCode;
	}

	public Switch(String switchIdentifier, String switchType) {
		this.switchIdentifier = switchIdentifier;
		this.switchType = switchType;
	}

	public Switch(String switchIdentifier, IOTProduct iotProduct) {
		this.switchIdentifier = switchIdentifier;
		this.iotProduct = iotProduct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public IOTProduct getIotProduct() {
		return iotProduct;
	}

	public void setIotProduct(IOTProduct iotProduct) {
		this.iotProduct = iotProduct;
	}

	public String getSwitchType() {
		return switchType;
	}

	public void setSwitchType(String switchType) {
		this.switchType = switchType;
	}

	public String getDimmerStatus() {
		return dimmerStatus;
	}

	public void setDimmerStatus(String dimmerStatus) {
		this.dimmerStatus = dimmerStatus;
	}

	public String getDimmerValue() {
		return dimmerValue;
	}

	public void setDimmerValue(String dimmerValue) {
		this.dimmerValue = dimmerValue;
	}

	public String getSwitchStatus() {
		return switchStatus;
	}

	public void setSwitchStatus(String switchStatus) {
		this.switchStatus = switchStatus;
	}

	public Integer getSwitchNumber() {
		return switchNumber;
	}

	public void setSwitchNumber(Integer switchNumber) {
		this.switchNumber = switchNumber;
	}

	public String getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getHideStatus() {
		return hideStatus;
	}

	public void setHideStatus(String hideStatus) {
		this.hideStatus = hideStatus;
	}

	public String getSwitchIdentifier() {
		return switchIdentifier;
	}

	public void setSwitchIdentifier(String switchIdentifier) {
		this.switchIdentifier = switchIdentifier;
	}

	public Integer getSwitchImageId() {
		return switchImageId;
	}

	public void setSwitchImageId(Integer switchImageId) {
		this.switchImageId = switchImageId;
	}

	public String getLockCode() {
		return lockCode;
	}

	public void setLockCode(String lockCode) {
		this.lockCode = lockCode;
	}

	@Override
	public String toString() {
		return "Switch [id=" + id + ", switchIdentifier=" + switchIdentifier + ", switchNumber=" + switchNumber
				+ ", switchType=" + switchType + ", dimmerStatus=" + dimmerStatus + ", dimmerValue=" + dimmerValue
				+ ", switchStatus=" + switchStatus + ", lockStatus=" + lockStatus + ", hideStatus=" + hideStatus
				+ ", switchName=" + switchName + ", lockCode=" + lockCode + ", switchImageId=" + switchImageId
				+ ", iotProduct=" + iotProduct + "]";
	}
	


}
