package com.gsmart.constants;

public class Constants {

	public static final String GENIE_TOWER_INFO = "towerInfo";

	public static final String GENIE_CAM_LIST = "camList";
	public static final String GENIE_CAM_ID = "genieCamId";
	public static final String GENIE_CAM_NAME = "camName";
	public static final String GENIE_LOCAL_IP = "localIP";
	public static final String GENIE_LOCAL_PORT = "localPort";
	public static final String GENIE_INTERNET_IP = "internetIP";
	public static final String GENIE_INTERNET_PORT = "internetPort";

	public static final String GENIE_CAM_LIST_STATUS = "GENIE_CAM_LIST";
	public static final String GENIE_CAM_ADD_STATUS = "GENIE_CAM_ADD";
	public static final String GENIE_CAM_EDIT_STATUS = "GENIE_CAM_EDIT";
	public static final String GENIE_CAM_DELETE_STATUS = "GENIE_CAM_DELETE";

	public static final String GENIE_SWITCH_STATUS = "0";
	public static final String GENIE_DIMMER_STATUS = "1";
	public static final String GENIE_CURTAIN_STATUS = "2";
	public static final String GENIE_LOCK_STATUS = "3";
	public static final String GENIE_MAIN_LOCK_STATUS = "4";
	public static final String GENIE_COLOUR_STATUS = "5";

	public static final String NOT_HIDDEN = "0";
	public static final String HIDDEN = "1";

	public static final String GENIE_COLOR_STATUS = "Genie_Color";
	public static final String GENIE_COLOR = "genieColor";
	public static final String ACTIVITY_LIST = "activityList";
	public static final String MESSAGE_FROM_INTERNET = "Internet";

	public static final int HOME_ARMED = 1;
	public static final int HOME_NOT_ARMED = 0;

	public static final String HOME_ARMED_MESSAGE = " has turned Home Armed. Your Home is Safe Now, whenever any user does physical Switch ON or OFF you will be notified";
	public static final String HOME_DISARMED_MESSAGE = " has turned Home Unarmed Successfully.";

	public static final String BACK_TO_ONLINE_MESSAGE = " is back to online now enjoy your Smart Home Service";

	public static final String IS_ARMED = "isArmed";

	public static final int HOME_BLOCKED = 1;
	public static final int HOME_NOT_BLOCKED = 0;

	public static final int SUNDAY = 1;
	public static final int MONDAY = 2;
	public static final int TUESDAY = 3;
	public static final int WEDNESDAY = 4;
	public static final int THURSDAY = 5;
	public static final int FRIDAY = 6;
	public static final int SATURDAY = 7;

	public static final String DIMMER_STATUS_FOR_NORMAL_SWITCH = "0";
	public static final String DIMMER_STATUS_FOR_DIMMER = "1";
	public static final String DIMMER_STATUS_FOR_CURTAIN = "2";
	public static final String DIMMER_STATUS_FOR_LOCK = "3";
	public static final String DIMMER_STATUS_FOR_HOME_LOCK = "4";
	public static final String DIMMER_STATUS_FOR_GENIE_COLOR = "5";

	public static final String SWITCH_ON = "1";
	public static final String SWITCH_OFF = "0";

	public static final String FAILURE_MSG = "Failure";
	public static final String EXCEPTION_MSG = "Exception";
	public static final String SUCCESS_MSG = "Success";
	public static final String REFRESH_BACK = "refreshBack";
	public static final String TOPIC_TOWER = "tower";

	public static final String PANEL_NOT_FOUND = "Panel Not Found";

	// Delivery Message in MQTT Quality of Service(Qos)
	public static final int QoS_ATMOST_ONCE = 0;
	public static final int QoS_ATLEAST_ONCE = 1;
	public static final int QoS_EXACTLY_ONCE = 2;

	public static final String SWITCH_ON_OFF_MESSAGE_TYPE = "Switch_ON_OFF";
	public static final String SCHEDULAR_SWITCH_ON_OFF_MESSAGE_TYPE = "Schedular_Switch_ON_OFF";

	public static final String SWITCH_NAME_UPDATE_MESSAGE_TYPE = "Switch_Name_update";
	public static final String SWITCH_LOCK_UNLOCK_MESSAGE_TYPE = "Switch_Lock_Unlock";

	public static final String SWITCH_STATUS_ON = "on";
	public static final String SWITCH_STATUS_OFF = "off";

	public static final String LOCK = "lock";
	public static final String UNLOCK = "unlock";

	public static final String UNHIDE = "unhide";
	public static final String HIDE = "hide";

	// JSON Param
	public static final String SWITCH_ID = "switchid";
	public static final String PROFILE_SWITCH_ID = "profileSwitchid";
	public static final String PROFILE_ON_OFF_STATUS = "profileOnOffStatus";
	public static final String FAILURE = "failure";
	public static final String LOCKBIT = "lockbit";
	public static final String SWITCH_NUMBER = "switchnumber";
	public static final String SWITCH_NAME = "switchname";
	public static final String SWITCH_STATUS = "switchstatus";
	public static final String DIMMER_STATUS = "dimmerstatus";
	public static final String DIMMER_VALUE = "dimmervalue";
	public static final String ACTIVITY_ID = "activityid";
	public static final String LOCK_STATUS = "lockstatus";
	public static final String LOCK_CODE = "lockCode";
	public static final String MESSAGE_FROM = "messageFrom";
	public static final String USER_IMAGE = "userimage";
	public static final String ROOM_ID = "RoomId";
	public static final String ROOM_NAME = "RoomName";
	public static final String TIME = "Time";
	public static final String STATUS = "status";
	public static final String USER_ID = "userId";
	public static final String USER_NAME = "username";
	public static final String MESSAGE = "message";
	public static final String SWITCH_IMAGE_ID = "switchimageId";
	public static final String HIDE_STATUS = "hidestatus";
	public static final String SCHEDULAR_ID = "schedulerid";
	public static final String SCHEDULAR_DATE_TIME = "scheduleDateTime";
	public static final String ROOM_LIST = "Roomlist";
	public static final String SWITCH_LIST = "Switchlist";
	public static final String SCHEDULE_LIST = "scheduleList";
	public static final String SCHEDULE_PROFILE_LIST = "scheduleProfileList";
	public static final String ROOM_IMAGE_ID = "roomimageId";
	public static final String SWITCH_AND_ROOM_LIST = "switchAndRoomList";
	public static final String REPEAT_STATUS = "repeatStatus";
	public static final String REPEAT_WEEK = "repeatWeek";

	// Profile Mode
	public static final String PROFILE_LIST = "profileList";
	public static final String PROFILE_NAME = "profileName";
	public static final String PROFILE_ID = "profileId";

	// Months name
	public static final String JAN = "Jan";
	public static final String FEB = "Feb";
	public static final String MAR = "Mar";
	public static final String APR = "Apr";
	public static final String MAY = "May";
	public static final String JUN = "June";
	public static final String JUL = "July";
	public static final String AUG = "Aug";
	public static final String SEP = "Sept";
	public static final String OCT = "Oct";
	public static final String NOV = "Nov";
	public static final String DEC = "Dec";

	// UserDetailsServiceIMPL Constants
	public static final String USER_TYPE_ADMIN = "Admin";
	public static final String USER_TYPE_NORMAL = "Normal";
	public static final String USER_TYPE_MODERATE = "Moderate";

	// FCMNotifier
	public static final String FCM_NOTIFIER_TITLE = "title";
	public static final String FCM_NOTIFIER_BODY = "body";
	public static final String FCM_NOTIFIER_TO = "to";
	public static final String FCM_NOTIFIER_DATA = "data";
	public static final String FCM_NOTIFIER_CONTENT_AVAILABLE = "content_available";
	public static final String FCM_NOTIFIER_SOUND = "sound";
	public static final String FCM_NOTIFIER_NOTIFICATION = "notification";
	public static final String FCM_NOTIFIER_URL = "https://fcm.googleapis.com/fcm/send";

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String AUTHORISATION = "Authorization";

	// public static final String NOTIFICATION_SERVER_KEY =
	// "AIzaSyBx6fKJ_LSGqZkLBCzApISB40hZlIqm6K4";
	public static final String NOTIFICATION_SERVER_KEY = "AIzaSyDnTWjcMIFuDR_bKfcp1AAkW5xua6x0mlw";

	public static final String NOT_APPLICABLE = "NA";

}
