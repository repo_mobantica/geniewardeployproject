package MqttReadDataPaho;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import com.gsmart.controller.DeployWarFile;
import com.gsmart.controller.RestartContoller;

public class MqttIntConnectionToHub implements MqttCallback {
	String subscribePanel;
	private String clientId = "119";
	private static MqttClient mqttConnection = null;
	private static String broker = "tcp://103.12.211.52:1883";
	//private static String broker = "tcp://192.168.0.18:1883";
	private static MqttIntConnectionToHub currObj = null;


	public MqttIntConnectionToHub() {
	}

	public MqttIntConnectionToHub(String csubscribePanel) {
		subscribePanel = csubscribePanel;
	}

	public MqttClient getMqttConnection() {

		if (mqttConnection != null && mqttConnection.isConnected()) {
			/*
			try {
				mqttConnection.disconnect();
			}catch (Exception e) {
				// TODO: handle exception
			}
			 */
			//getObj().getMqttConnection();
			System.out.println("reusing connection...to Genie Central");
			return mqttConnection;
		} else 

		{
			try {

				JdbcConnection JdbcConnection=new JdbcConnection();
				String home_id=JdbcConnection.HomeId();
				clientId=mqttConnection.generateClientId();
				MemoryPersistence persistence = new MemoryPersistence();
				mqttConnection = new MqttClient(broker,clientId, persistence);
				MqttConnectOptions connOpts = new MqttConnectOptions();
				connOpts.setKeepAliveInterval(10);
				connOpts.setAutomaticReconnect(true);
				connOpts.setCleanSession(true);
				mqttConnection.connect(connOpts);

				mqttConnection.subscribe("restartHomeId_"+home_id);
				mqttConnection.setCallback(this);

				System.out.println("Connected");
				return mqttConnection;
			} catch (MqttException exception) {
				try {
					System.out.println("MqttException exception = "+exception);
					System.out.println("unable to connect Genie Central");
					System.out.println("trying...");
					Thread.sleep(3000);
					MqttIntConnectionToHub.getObj().getMqttConnection();
					exception.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return mqttConnection;
			}
		}
	}


	public static MqttIntConnectionToHub getObj() {
		if (currObj == null) {
			currObj = new MqttIntConnectionToHub();
			return currObj;
		} else
			return currObj;
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(String arg0, MqttMessage mqttmessage) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Topic name ="+arg0);
		System.out.println("mqttmessage= "+mqttmessage);

		try {
			JdbcConnection JdbcConnection=new JdbcConnection();
			String home_id=JdbcConnection.HomeId();
			String topic="GenieHomeId";
			//reboot
			if(mqttmessage.toString().equalsIgnoreCase("wish1"))
			{
				String msg1=RestartContoller.RestartTomcatFunction();

				String msg="wish/reboot/"+msg1+"/"+home_id;
				MqttMessage messageOff = new MqttMessage(msg.toString().getBytes());
				messageOff.setQos(2);
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(topic, messageOff);

			}
			//checkLocalHomeConnection
			if(mqttmessage.toString().equalsIgnoreCase("wish2"))
			{
				String msg="wish/checkLocalHomeConnection/2/"+home_id;
				MqttMessage messageOff = new MqttMessage(msg.toString().getBytes());
				messageOff.setQos(2);
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(topic, messageOff);

			}
			//deployWarFile
			if(mqttmessage.toString().equalsIgnoreCase("wish3"))
			{

				DeployWarFile deployWarFile=new DeployWarFile();
				String msg1=deployWarFile.DeployWarFile();

				String msg="wish/deployWarFile/"+msg1+"/"+home_id;
				MqttMessage messageOff = new MqttMessage(msg.toString().getBytes());
				messageOff.setQos(2);
				MqttIntConnectionToHub.getObj().getMqttConnection().publish(topic, messageOff);

			}
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}
}
